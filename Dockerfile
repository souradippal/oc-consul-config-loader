FROM alpine:3.5
MAINTAINER souradip.pal@gmail.com

RUN apk --update add nodejs git openssh curl bash inotify-tools jq && \
    rm -rf /var/cache/apk/* && \
    npm install -g simplywatch@2.5.7 && \
    npm install -g git2consul@0.12.13 && \
    mkdir -p /etc/git2consul.d

ADD /load-config.sh /
VOLUME /config

ENV CONFIG_MODE=filesystem
ENV INIT_SLEEP_SECONDS=5
ENV CONSUL_URL=localhost
ENV CONSUL_PORT=8500
ENV CONFIG_DIR=/config

RUN chgrp -R 0 /usr/lib && \
    chmod -R g+rwX /usr/lib && \
	chmod -R g=u /usr/lib
	
RUN chgrp -R 0 /load-config.sh && \
    chmod -R g+rwX /load-config.sh && \
	chmod -R g=u /load-config.sh

CMD /load-config.sh
